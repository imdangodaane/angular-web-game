import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import * as jwtDecode from 'jwt-decode';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  login = {
    username: '',
    password: '',
  };

  constructor(
    private userService: UserService,
    private cookieService: CookieService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit() {
    const payload = {
      ...this.login,
    };
    this.userService.login(payload).pipe(
      map((res: { token: string }) => {
        const dataCtx: any = jwtDecode(res.token);
        const expireTime = new Date(dataCtx.exp);
        this.cookieService.set('token', res.token, expireTime);
        this.authenticationService.onLogin(res);
      }),
      map(() => {
        this.router.navigateByUrl(this.returnUrl);
      }),
      catchError((err) => {
        return of(null);
      }),
    ).subscribe();
  }

}
