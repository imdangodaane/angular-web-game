import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from '../login/login.component';
import { AuthenticationService } from '../../services/authentication.service';
import * as jwtDecode from 'jwt-decode';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  loginModal: NgbModalRef;
  userInfo = null;

  constructor(
    private modalService: NgbModal,
    private authenticationService: AuthenticationService,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.authenticationService.recheck();
      }
    });
    this.handleBarWhenUserLogin();
  }

  openLogin() {
    this.loginModal = this.modalService.open(LoginComponent);
  }

  handleBarWhenUserLogin(): void {
    this.authenticationService.userToken.subscribe(
      (dto: { token: string }) => {
        let decodedDto: any;
        try {
          decodedDto = jwtDecode(dto.token);
          this.userInfo = {
            username: decodedDto.username,
          };
          try {
            this.loginModal.dismiss();
          } catch (err) {}
        } catch (err) {
          this.userInfo = null;
        }
      },
      (err: Error) => {
        console.error(err);
      },
    );
  }

  logout() {
    this.authenticationService.onLogout();
    this.userInfo = null;
    window.location.reload();
  }

}
