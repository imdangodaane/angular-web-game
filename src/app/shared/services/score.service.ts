import { Injectable } from '@angular/core';
import { API } from 'src/app/config/api.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class ScoreService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${this.authenticatioService.getUserTokenValue()}`,
    }),
  };

  constructor(
    private http: HttpClient,
    private authenticatioService: AuthenticationService,
  ) {}

  updateScoreToServer(score: number) {
    const url = API.SCORE.FLAPPY_BIRD;
    const payload = {
      score,
    };
    console.log(url, payload);
    console.log(this.httpOptions);
    return this.http.post(url, payload, this.httpOptions);
  }

  getTopScore() {
    const url = API.SCORE.FLAPPY_BIRD;
    return this.http.get(url);
  }
}
