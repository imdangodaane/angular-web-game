import { Injectable } from '@angular/core';
import { ValidatorFn, AbstractControl } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ValidatorService {

  constructor() { }

  matchPassword(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const match = control.value.password === control.value.rePassword;
      return match ? null : { passwordNotMatch: true };
    };
  }
}
