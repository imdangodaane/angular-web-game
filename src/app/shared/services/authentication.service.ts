import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  userToken: BehaviorSubject<object> = new BehaviorSubject(undefined);

  constructor(
    private cookieService: CookieService,
  ) {}

  onLogin(dto: { token: string }) {
    this.userToken.next(dto);
  }

  onLogout() {
    this.cookieService.delete('token');
    this.userToken.next(null);
  }

  recheck() {
    this.userToken.next({
      token: this.cookieService.get('token'),
    });
  }

  getUserTokenValue() {
    this.recheck();
    return this.userToken.getValue()['token'];
  }
}
