import { Injectable } from '@angular/core';
import { IUserRegister, IUserLogin } from 'src/app/models/user.interface';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/config/api.constant';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) { }

  register(payload: IUserRegister): Observable<any> {
    const url = API.USER.REGISTER;
    return this.http.post(url, payload);
  }

  login(payload: IUserLogin): Observable<any> {
    const url = API.USER.LOGIN;
    return this.http.post(url, payload);
  }
}
