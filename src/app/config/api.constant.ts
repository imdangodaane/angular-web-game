const BASE_URL = `http://localhost:4001`;
const PREFIX = `api`;
const USER = `user`;
const SCORE = `score`;

export const API = {
  USER: {
    REGISTER: `${BASE_URL}/${USER}/register`,
    LOGIN: `${BASE_URL}/${USER}/login`,
  },
  SCORE: {
    FLAPPY_BIRD: `${BASE_URL}/${SCORE}/flappybird`,
  },
};
