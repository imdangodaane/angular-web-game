import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ValidatorService } from 'src/app/shared/services/validator.service';
import { UserService } from 'src/app/shared/services/user.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  registerForm = this.formBuilderService.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
    rePassword: [''],
    email: ['', [Validators.required, Validators.email]],
  }, {
    validators: [this.validatorService.matchPassword()],
  });

  constructor(
    private formBuilderService: FormBuilder,
    private validatorService: ValidatorService,
    private userService: UserService,
  ) {}

  ngOnInit(): void {
  }

  onSubmit() {
    const payload = {
      username: this.registerForm.get('username').value,
      password: this.registerForm.get('password').value,
      email: this.registerForm.get('email').value,
    };
    this.userService.register(payload).pipe(
      map((res) => {
        console.log(res);
      }),
      catchError((err) => {
        console.error(err);
        return of(null);
      }),
    ).subscribe();
  }

}
