import { Component, OnInit, OnDestroy } from '@angular/core';
import phaser from 'phaser';
import { BehaviorSubject } from 'rxjs';
import { ScoreService } from 'src/app/shared/services/score.service';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

const score = 0;

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit, OnDestroy {
  phaserGame: phaser.Game;
  config: phaser.Types.Core.GameConfig;
  topScore: any;

  constructor(
    private scoreService: ScoreService,
  ) {
    this.config = {
      type: phaser.AUTO,
      width: 900,
      height: 500,
      parent: 'gameContainer',
      physics: {
        default: 'arcade',
        arcade: {
          gravity: { y: 500 },
        },
      },
    };
  }

  ngOnInit(): void {
    const mainScene = new MainScene(this.scoreService);
    this.config.scene = [mainScene];
    this.phaserGame = new phaser.Game(this.config);
    this.getTopScore();
  }

  ngOnDestroy() {
    this.phaserGame.destroy(true);
  }

  getTopScore() {
    this.scoreService.getTopScore().subscribe((res: any) => {
      this.topScore = res.sort((a: any, b: any) => a.games.score < b.games.score ? 1 : -1);
      console.log(this.topScore);
    });
  }
}

class MainScene extends Phaser.Scene {
  platforms: any;
  pipe1: phaser.Physics.Arcade.Group;
  posX = 950;
  bird: phaser.Physics.Arcade.Sprite;
  score = 0;
  scoreText: any;
  speed = 5;
  scoreSubject: BehaviorSubject<string> = new BehaviorSubject('0');
  scoreService: any;

  constructor(
    scoreService: ScoreService,
  ) {
    super({ key: 'main' });
    this.scoreService = scoreService;
    this.updateScoreToServerInitializer();
  }

  create() {
    this.add.sprite(450, 250, 'background');
    this.scoreText = this.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#000' });

    this.bird = this.physics.add.sprite(30, 250, 'bird');
    // this.bird.setCollideWorldBounds(true);

    const upperHeadPipe = this.physics.add.sprite(450, 320, 'upper-head-pipe').setOrigin(0.5, 0).setScale(0.2, 0.32);
    const lowerHeadPipe = this.physics.add.sprite(450, -100, 'lower-head-pipe').setOrigin(0.5, 0).setScale(0.2, 0.32);
    this.pipe1 = this.physics.add.group({
      immovable: true,
      allowGravity: false,
    });
    this.pipe1.add(upperHeadPipe);
    this.pipe1.add(lowerHeadPipe);

    this.pipe1.setX(this.posX);

    // this.platforms = this.physics.add.staticGroup();

    this.physics.add.overlap(this.bird, this.pipe1, this.gameOver, null, this);
  }

  preload() {
    this.load.image('background', 'assets/phaser/flappy-bird/images/background.png');
    this.load.image('bird', 'assets/phaser/flappy-bird/images/bird.png');
    this.load.image('pipe', 'assets/phaser/flappy-bird/images/pipe.png');
    this.load.image('upper-head-pipe', 'assets/phaser/flappy-bird/images/upper-head-pipe.png');
    this.load.image('lower-head-pipe', 'assets/phaser/flappy-bird/images/lower-head-pipe.png');
  }

  update() {
    this.posX -= this.speed;
    this.pipe1.setX(this.posX);
    if (this.posX < -50) {
      this.posX = 950;
      this.score += 10;
      this.scoreSubject.next(String(this.score));
      this.scoreText.setText(`Score: ${this.score}`);
      const randomNumber = ((Math.random() * 2) - 1) * 75;
      phaser.Actions.Call(this.pipe1.getChildren(), (child: any) => {
        child.setY(child.y + randomNumber);
      }, this);
    }

    if (this.bird.y > 500 || this.bird.y < -50) {
      this.gameOver();
    }

    const cursors = this.input.keyboard.createCursorKeys();

    if (cursors.up.isDown) {
      this.bird.setVelocityY(-150);
    }
  }

  gameOver() {
    alert('Game Over');
    this.scene.restart();
    this.posX = 950;
    this.score = 0;
  }

  updateScoreToServerInitializer() {
    this.scoreSubject.subscribe((scoreString: string) => {
      console.log(scoreString);
      this.scoreService.updateScoreToServer(scoreString).subscribe(
        (res) => {
          console.log(res);
          console.log('Updated score to server.');
        },
      );
    });
  }
}
