import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HomepageComponent } from './homepage/homepage.component';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { GamesComponent } from './games/games.component';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomepageComponent,
    GamesComponent,
    RegisterComponent,
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedComponentsModule,
    ReactiveFormsModule,
  ],
})
export class PagesModule { }
